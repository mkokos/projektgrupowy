package pl.wspa.grupowy.projekt.mobilne.tank;

import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.Context;
import android.net.wifi.WifiConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;


public class Main extends Activity {
    RelativeLayout layout_joystickRight, layout_joystickLeft;
    ImageView image_joystick, image_border;
    TextView textView, textView1, textView2, textView3, textView4, textView5;
    WifiManager mWifiManager;
    JoyStickClass jsR, jsL;
    Socket socket = null;
    final Handler handler = new Handler();
    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            runUdpClient();
        }
    });

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

//        textView=(TextView)findViewById(R.id.textView1);
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        textView = (TextView) findViewById(R.id.wifiinfo);
        textView.setText("Łączenie...");
        conectToWifi("TankWiFi", "12345678");
        textView.setText("Połaczono");

        startClientThred();
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
//		final String text;
        layout_joystickRight = (RelativeLayout) findViewById(R.id.layout_joystickRight);
        layout_joystickLeft = (RelativeLayout) findViewById(R.id.layout_joystickLeft);

        jsR = new JoyStickClass(getApplicationContext()
                , layout_joystickRight, R.drawable.image_button);
        jsR.setStickSize(50, 50);
        jsR.setLayoutSize(300, 300);
        jsR.setLayoutAlpha(150);
        jsR.setStickAlpha(100);
        jsR.setOffset(20);
        jsR.setMinimumDistance(20);

        layout_joystickRight.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                jsR.drawStick(arg1);
                if (arg1.getAction() == MotionEvent.ACTION_DOWN
                        || arg1.getAction() == MotionEvent.ACTION_MOVE) {
                    textView1.setText("X : " + String.valueOf(jsR.getX()));
                    textView2.setText("Y : " + String.valueOf(jsR.getY()));
                    textView3.setText("Angle : " + String.valueOf(jsR.getAngle()));
                    textView4.setText("Distance : " + String.valueOf(jsR.getDistance()));
                    textView.setText(catchValue());

                    int direction = jsR.get8Direction();
                    if (direction == JoyStickClass.STICK_UP) {
                        textView5.setText("Direction : Up");
                    } else if (direction == JoyStickClass.STICK_UPRIGHT) {
                        textView5.setText("Direction : Up Right");
                    } else if (direction == JoyStickClass.STICK_RIGHT) {
                        textView5.setText("Direction : Right");
                    } else if (direction == JoyStickClass.STICK_DOWNRIGHT) {
                        textView5.setText("Direction : Down Right");
                    } else if (direction == JoyStickClass.STICK_DOWN) {
                        textView5.setText("Direction : Down");
                    } else if (direction == JoyStickClass.STICK_DOWNLEFT) {
                        textView5.setText("Direction : Down Left");
                    } else if (direction == JoyStickClass.STICK_LEFT) {
                        textView5.setText("Direction : Left");
                    } else if (direction == JoyStickClass.STICK_UPLEFT) {
                        textView5.setText("Direction : Up Left");
                    } else if (direction == JoyStickClass.STICK_NONE) {
                        textView5.setText("Direction : Center");
                    }
                } else if (arg1.getAction() == MotionEvent.ACTION_UP) {
                    textView1.setText("X :");
                    textView2.setText("Y :");
                    textView3.setText("Angle :");
                    textView4.setText("Distance :");
                    textView5.setText("Direction :");
                    textView.setText(catchValue());
//					sendMessage(textView.getText().toString());
                }
                return true;
            }
        });
        jsL = new JoyStickClass(getApplicationContext()
                , layout_joystickLeft, R.drawable.image_button);
        jsL.setStickSize(50, 50);
        jsL.setLayoutSize(300, 300);
        jsL.setLayoutAlpha(150);
        jsL.setStickAlpha(100);
        jsL.setOffset(20);
        jsL.setMinimumDistance(20);

        layout_joystickLeft.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                jsL.drawStick(arg1);
                if (arg1.getAction() == MotionEvent.ACTION_DOWN
                        || arg1.getAction() == MotionEvent.ACTION_MOVE) {
                    textView1.setText("X : " + String.valueOf(jsL.getX()));
                    textView2.setText("Y : " + String.valueOf(jsL.getY()));
                    textView3.setText("Angle : " + String.valueOf(jsL.getAngle()));
                    textView4.setText("Distance : " + String.valueOf(jsL.getDistance()));
                    textView.setText(catchValue());

                    int direction = jsL.get8Direction();
                    if (direction == JoyStickClass.STICK_UP) {
                        textView5.setText("Direction : Up");
                    } else if (direction == JoyStickClass.STICK_UPRIGHT) {
                        textView5.setText("Direction : Up Right");
                    } else if (direction == JoyStickClass.STICK_RIGHT) {
                        textView5.setText("Direction : Right");
                    } else if (direction == JoyStickClass.STICK_DOWNRIGHT) {
                        textView5.setText("Direction : Down Right");
                    } else if (direction == JoyStickClass.STICK_DOWN) {
                        textView5.setText("Direction : Down");
                    } else if (direction == JoyStickClass.STICK_DOWNLEFT) {
                        textView5.setText("Direction : Down Left");
                    } else if (direction == JoyStickClass.STICK_LEFT) {
                        textView5.setText("Direction : Left");
                    } else if (direction == JoyStickClass.STICK_UPLEFT) {
                        textView5.setText("Direction : Up Left");
                    } else if (direction == JoyStickClass.STICK_NONE) {
                        textView5.setText("Direction : Center");
                    }
                } else if (arg1.getAction() == MotionEvent.ACTION_UP) {
                    textView1.setText("X :");
                    textView2.setText("Y :");
                    textView3.setText("Angle :");
                    textView4.setText("Distance :");
                    textView5.setText("Direction :");
                    textView.setText(catchValue());
//					sendMessage(textView.getText().toString());
                }
                return true;
            }
        });
    }

    private static void writeCommand(Socket socket, String command) {
        // If no socket is null return
        if (socket == null)
            return;
        // if socket is connected send command
        if (socket.isConnected()) {
            byte[] cmdByte = new byte[0];
            try {
                cmdByte = new byte[command.getBytes("US-ASCII").length];
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

//            byte[] temp;
            try {
//                temp = ";".getBytes("US-ASCII");
                cmdByte = command.getBytes("US-ASCII");

            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("Error");
            }
            try {
                socket.getOutputStream().write(cmdByte);
                System.out.println("Message sent");
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
                System.out.println("Error");
            }
        }
    }

    private void conectToWifi(final String nSSID, final String nPASS) {
        if (!mWifiManager.isWifiEnabled()) {
            mWifiManager.setWifiEnabled(true);
        }
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = String.format("\"%s\"", nSSID);
        conf.preSharedKey = String.format("\"%s\"", nPASS);

        int netId = mWifiManager.addNetwork(conf);
        mWifiManager.disconnect();
        mWifiManager.enableNetwork(netId, true);
        mWifiManager.reconnect();
        BufferedReader in = null;
        PrintWriter out = null;
//		String serverAddress = "192.168.1.1";
//		try {
//			socket = new Socket(serverAddress, 8888);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		try {
//			if (socket != null) {
//				in = new BufferedReader(new InputStreamReader(
//						socket.getInputStream()));
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		try {
//			if (socket != null) {
//				out = new PrintWriter(socket.getOutputStream(), true);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
    }


    int cutOff(int liczba, int dGranica, int gGranica) {
        if (liczba > gGranica)
            return gGranica;
        else if (liczba < dGranica)
            return dGranica;
        else
            return liczba;
    }

    ;

    int notLess(int liczba, int min) {
        if (liczba < min)
            return min;
        else
            return liczba;
    }

    ;

    int notMore(int liczba, int max) {
        if (liczba < max)
            return max;
        else
            return liczba;
    }

    ;

    String speedToString(int lSpeed, int rSpeed) {
        return "" + lSpeed + "," + rSpeed + ",";
    }

    ;

    String decodeSpeed(int valX, int valY, int upLimit, int downLimit, int zeroOffset) {
        valY = cutOff(valY, upLimit, downLimit);
        valX = cutOff(valX, upLimit, downLimit);
//
//       2    |    1
//            |
//        ^^^^^^^^^
//            |
//       3    |    4
//        |=0
        if (valX < zeroOffset && valX > -zeroOffset) //foward&backward
            return speedToString(valY, valY);//      0


        if (valX <= -zeroOffset) {
            if (valY >= -zeroOffset) {
                if (valY < zeroOffset) {
                    valY = 0;
                }
                System.out.println("1");
                return speedToString(upLimit - valX,notLess(valY, valX));
            }//   1
            else {
                System.out.println("3");
                return speedToString(valY + valX, notMore(valY, -valX));//     3
            }
        } else if (valX > zeroOffset) {
            if (valY < -zeroOffset) {
                System.out.println("4");
                return speedToString(notMore(valY, valX), valY + valX);

            } //   4
            else {
                System.out.println("2");
                return speedToString(upLimit - valY, notLess(valY, -valX));

            }//    2

        }

        return "";
    }

    String catchValue() {

        Random generator = new Random();
        int liczba = 0, suma = 0;
        StringBuilder stringBuilder = new StringBuilder();
//		System.out.println("Lewa gonsiennica:");
//        liczba = -jsL.getY();
//        if (liczba < -200)
//            liczba = -200;
//        if (liczba > 200)
//            liczba = 200;
//        liczba += 200;
//        liczba *= 0.0375;
//        suma += liczba;
//        stringBuilder.append(liczba).append(',');
////		System.out.println("Prawa gonsiennica:");
//        liczba = -jsR.getY();
//        if (liczba < -200)
//            liczba = -200;
//        if (liczba > 200)
//            liczba = 200;
//        liczba += 200;
//        liczba *= 0.0375;
        int x, y;
        x = -jsL.getX();
        y = -jsL.getY();


        suma += x + y;
        stringBuilder.append(decodeSpeed(x, y, 100, -100, 20));
        System.out.println(stringBuilder.toString());
        for (int i = 2; i < 9; i++) {
            if (i >= 5 && i <= 7)
                liczba = generator.nextInt(255);
            else
                liczba = generator.nextInt(16);
            suma += liczba;
            stringBuilder.append(liczba).append(",");

        }
        liczba = suma % 16;
        stringBuilder.append(liczba);

//            stringBuilder.append(';');
        textView.setText(stringBuilder.toString());
        return stringBuilder.toString();

    }

    private void startClientThred() {

        thread.start();


    }

    ;

//    private void sendMessage() {
//        String newMesage = "";
//        String oldMesage = "Połaczono";
//        try {
//            while (true) {
//
//
//                //Replace below IP with the IP of that device in which server socket open.
//                //If you change port then change the port number in the server side code also.
//                newMesage = textView.getText().toString();
//                if (!oldMesage.equals(newMesage)) {
//                    Socket s = new Socket("192.168.1.1", 8888);
//
//                    OutputStream out = s.getOutputStream();
//
//                    PrintWriter output = new PrintWriter(out);
//
//                    System.out.println("soket");
//
//                    oldMesage = newMesage;
//                    output.println(oldMesage);
//                    output.flush();
//
//                    //					BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
//                    //					final String st = input.readLine();
//                    //
//                    //					handler.post(new Runnable() {
//                    //						@Override
//                    //						public void run() {
//                    //
//                    //							String s = mTextViewReplyFromServer.getText().toString();
//                    //							if (st.trim().length() != 0)
//                    //								mTextViewReplyFromServer.setText(s + "\nFrom Server : " + st);
//                    //						}
//                    //					});
//
//                    output.close();
//                    out.flush();
//                    out.close();
//                    s.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("*************socket wyjatek***************");
//            e.printStackTrace();
//            sendMessage();
//        }
//
//
//    }
//
//    ;

    private void runUdpClient() {


        String udpMsg = "";
        DatagramSocket ds = null;
        String newMesage = "";
        String oldMesage = "Połaczono";
        while (true) {

            //Replace below IP with the IP of that device in which server socket open.
            //If you change port then change the port number in the server side code also.
            newMesage = textView.getText().toString();
            if (!oldMesage.equals(newMesage)) {
                try {
                    oldMesage = newMesage;
                    udpMsg = newMesage;
                    ds = new DatagramSocket();
                    InetAddress serverAddr = InetAddress.getByName("192.168.1.1");
                    DatagramPacket dp;
                    dp = new DatagramPacket(udpMsg.getBytes(), udpMsg.length(), serverAddr, 8888);
                    ds.send(dp);
                    System.out.println("send");
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (ds != null) {
                        ds.close();
                        try {

                            Thread.sleep(200);

                        } catch (InterruptedException ex) {
                            System.out.println("I'm interrupted");
                        }
//                    Thread.currentThread().interrupt();
                    }
                }
            }


        }


    }

    ;
}